# v-calculate

A 'super calculator' library for v-refracta, intended to be able to implement any number of unusual "calculator buttons" like counters, time or date calculations, and unit conversions, as well as store an arbitrary number of result cells and link them together like spreadsheets.

The aim is to eventually use this library to create graphical applications, and demonstrate v-refracta's **mix-and-match application interface** concept where users can "brick" together calculations and interface widgets into application windows in whatever order they prefer, rather than having to accept some pre-designed arrangement of single-purpose "apps".
