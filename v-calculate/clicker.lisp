;;; clicker.lisp - counter clicker to replace "timber-count" (2019)
(defpackage :v-refracta.calculate.clicker
	(:shadowing-import-from :v-refracta.calculate.sheet
		#:make-cell #:cell-path #:save-cell #:load-cell
		#:calculate)
	(:use :common-lisp)
	(:local-nicknames (:sheet :v-refracta.calculate.sheet))
	(:export
		;; imported
		#:make-cell #:save-cell #:load-cell #:calculate
		
		#:save-to-disk #:current-clicker  ; #::*save-to-disk* #::*current-clicker*
		
		#:inc #:dec
))
(in-package :v-refracta.calculate.clicker)


;;; settings
(defparameter *save-to-disk* nil)
(defparameter *current-clicker* nil)

;; convenience setters
(defun current-clicker (&optional clicker)  ; return or select current counter
	(if (null clicker)     ; if not passsed CLICKER...
		*current-clicker*  ; get value
		(setf *current-clicker* clicker))) ; otherwise set value
(defun save-to-disk (&optional (true-false nil save-arg-p))
	(if (null save-arg-p)  ; if not passsed value...
		*save-to-disk*     ; get value
		(setf *save-to-disk* true-false))) ; otherwise set value

;;; clickers

(defun inc (&optional clicker)  ; increment clicker
	(let ((counter
		(if (null clicker)
			*current-clicker*
			clicker)))
	;; increment clicker, and save to disk if requested
	(sheet:calculate '1+ counter :save-cell *save-to-disk*)
))
(defun dec (&optional clicker)  ; decrement clicker
	(let ((counter
		(if (null clicker)
			*current-clicker*
			clicker)))
	(sheet:calculate '1- counter :save-cell *save-to-disk*)
))
