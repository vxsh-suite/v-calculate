;;; sheet.lisp - for calculator memory values and spreadsheet-style cells
;; it is not recommended to :use this package when its names are generic enough to probably clash
(defpackage :v-refracta.calculate.sheet
	;; (:shadowing-import-from )
	(:use :common-lisp)
	(:export
		;; #::*default-cell-path* #::*default-cell-file*
		#:cell #:cell-p #:make-cell
			#:cell-value #:cell-unit #:cell-equation #:cell-path
			#:reset-cell-equation
			#:save-cell #:load-cell
				#:reify-cell-0.2.2.0 #:unreify-cell-0.2.2.0
		#:calculate
))
(in-package :v-refracta.calculate.sheet)

;;; settings / defaults {{{
(defparameter *default-cell-path*
	(merge-pathnames "v-calculate/" (uiop:xdg-data-home)))
	;; by default, store serialised cells in ~/.local/share/v-calculate
(defparameter *default-cell-file* (make-pathname :name "result"))
;; there is no preference for saving to disk here because that will likely be handled in other modules
;;; }}}


;;; CELL - calculator result / spreadsheet cell
(defstruct (cell)  ; value unit equation path  {{{
	(value 0)  ; current value
	unit       ; dimension in which value is expressed - should be a symbol
	equation   ; S-expression entered to produce value
		;; technically speaking, VALUE is half an equation and EQUATION is the other half.
	(path      ; file path this cell should be serialised to - used for clicker, etc.
		(merge-pathnames *default-cell-file* *default-cell-path*))
	
	;; LATER:
	;; * when there's a calculator interface, this will be used to hold memory values
	;; * create some object printing wizardry that displays EQUATION really fancily - '(* 4 1) -> 4 = 4 × 1
	;; * figure out what package/library measurement units should come from
) ; }}}

(defun reset-cell-equation (cell)  ; flatten unwieldy expressions down to cell-value  {{{
	(let ((value
		(cell-value cell)))
	(setf (cell-equation cell) value) ; update cell equation
	value                             ; return cell value
)) ; }}}

(defun reify-cell-0.2.2.0 (cell) ; {{{
	;; this function is "forever" - it will be archived and re-imported in if the format changes in a future version
	(format nil "~S"
		(uiop:reify-simple-sexp
			(list
				'cell
				"0.2.2.0"  ; version this s-expression format was created in
				(cell-value cell)
				(cell-unit cell)
				(cell-equation cell)
				;; path is not saved here as the saved file should know where it is
			)))
) ; }}}

(defun unreify-cell-0.2.2.0 (cell-string &key path) ; {{{
	(let* (
		(cell-list
			(uiop:unreify-simple-sexp (read-from-string cell-string)))
		(cell-structure
			(make-cell
				:value    (nth 2 cell-list)
				:unit     (nth 3 cell-list)
				:equation (nth 4 cell-list)
				:path nil))
	)
	(unless (null path)
		(setf (cell-path cell-structure) path))
	
	cell-structure
)) ; }}}

(defun save-cell (cell)  ; write cell to disk  {{{
	(let* (  ; let, in order
		(path (cell-path cell))
		(save-directory (uiop:pathname-directory-pathname path))
		(cell-result (reify-cell-0.2.2.0 cell))
	)
	;; create all directories in path if they do not already exist
	(ensure-directories-exist save-directory)
	;; write cell to file
	(with-open-file (stream path :direction :output)
		(format stream cell-result))
	
	cell-result  ; return cell string written to file
)) ; }}}

(defun load-cell (path)  ; load cell from disk {{{
	(let* (  ; let, in order
		(cell-string (uiop:read-file-string path))
		(cell-structure (unreify-cell-0.2.2.0 cell-string :path path))
	)
	cell-structure
)) ; }}}

(defun calculate (operation cell &key by save-cell)  ; update cell with new calculation  {{{
;; OPERATION - the function to perform on the cell
;; CELL      - the CELL containing the operand [the 'cell-value]
;; BY       - additional term if the OPERATION takes one

	(let* (  ; let, in order
		(current-equation
			(if (null (cell-equation cell))
				(cell-value cell)       ; ex. 4
				(cell-equation cell)))  ; ex. (list + 4 1)
		(new-equation
			(if (null by)
				(list operation current-equation)
				(list operation current-equation by)))
		;; determine if OPERATION is a macro in order to get the correct function to call
		(macro-function (macro-function operation))
		(operation-function
			(if (null macro-function)
				operation
				macro-function))
		(cell-result
			;; this needs to be specified this way in case the operation was defined without a second operand
			(if (null by)
				(funcall operation-function (cell-value cell))
				(funcall operation-function (cell-value cell) by)))
		(save-path
			(cond  ; write cell to this path if SAVE-CELL is...
				((equal save-cell t)  (cell-path cell))  ; T - cell-path
				((or (pathnamep save-cell) (stringp save-cell))  ; a directory - that directory
					save-cell)
				(t nil)))  ; NIL - do not write cell
	)
	
	;; update cell with calculation
	(setf
		(cell-value cell)    cell-result   ; update cell value
		(cell-equation cell) new-equation) ; update equation
	;; LATER: allow using cell as BY, and tossing its equation into the first cell's equation
	
	;; write cell to file if requested
	(unless (null save-path)
		(save-cell cell))
	
	cell-result  ; return new cell value
)) ; }}}
